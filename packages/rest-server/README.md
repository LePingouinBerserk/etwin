# Eternal-Twin REST interface

HTTP REST interface for the Eternal-Twin API.

Creates a Koa router from an API implementation.

## Actions

- `yarn build`: Compile the library
- `yarn test`: Compile the tests and run them
- `yarn lint`: Check for common errors and style issues.
- `yarn format`: Attempt to fix style issues automatically.
- `yarn start`: Compile and run a standalone REST server using the in-memory implementation.
